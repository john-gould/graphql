namespace Jdev.JohnGould.GraphQL.Api;

using Types;
using Jdev.JohnGould.Proto.Blog.Grpc.Markdown;
using Grpc.Net.Client;

public class Query
{
    public Blog GetBlog(string name)
    {
        var channel = GrpcChannel.ForAddress("http://localhost:5291");
        var client = new MarkdownService.MarkdownServiceClient(channel);
        var response = client.GetJson(new GetJsonRequest { Name = name });
        return new Blog { Json = response.Json };
    }
}